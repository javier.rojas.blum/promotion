package com.example.promotion.service;

import com.example.promotion.model.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ClientServiceTest {

    @Autowired
    ClientService clientService;

    @Test
    void findByBirthDateMonthAndBirthDateDay() {
        LocalDate localDate = LocalDate.now();

        int month = localDate.getMonthValue();
        int day = localDate.getDayOfMonth();

        List<Client> clients = clientService.getByMonthAndDay(localDate);

        assertNotNull(clients);
        assertTrue(clients.size() > 0);

        System.out.println("Clients found: " + clients.size());
        for (Client client : clients) {
            System.out.println(client);

            Date birthdate = client.getBirthDate();
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(birthdate);

            assertEquals(month, calendar.get(Calendar.MONTH) + 1);
            assertEquals(day, calendar.get(Calendar.DAY_OF_MONTH));
        }
    }
}