package com.example.promotion;

import com.example.promotion.model.Client;
import com.example.promotion.model.Promotion;
import com.example.promotion.repository.ClientRepository;
import com.example.promotion.repository.PromotionRepository;
import com.github.javafaker.Faker;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Date;
import java.util.GregorianCalendar;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private ClientRepository clientRepository;
    private PromotionRepository promotionRepository;

    public DevBootstrap(ClientRepository clientRepository, PromotionRepository promotionRepository) {
        this.clientRepository = clientRepository;
        this.promotionRepository = promotionRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        createClients();
        createPromotions();
    }

    private void createPromotions() {
        Promotion promotion = new Promotion();
        promotion.setSubject("Promocion <discountValue>% !!!");
        promotion.setDescription("Hola <name>. Hoy es su cumpleaños y usted es importante para nosotros, queremos " +
                "celebralo ofreciendo un <discountValue>% de descuento y delivery gratuito. Valido por 24 hrs.");

        promotionRepository.save(promotion);
    }

    private void createClients() {
        Faker faker = new Faker();

        LocalDate localDate = LocalDate.now();
        GregorianCalendar gregorianCalendar = new GregorianCalendar(1983, localDate.getMonthValue() - 1, localDate.getDayOfMonth());

        createClient(
                "Javier Rojas",
                gregorianCalendar.getTime(),
                "javier.rojas.blum@gmail.com"
        );

        for (int i = 0; i < 10; i++) {
            createClient(
                    faker.name().fullName(),
                    faker.date().birthday(18, 65),
                    faker.internet().emailAddress()
            );
        }
    }

    private void createClient(String name, Date birthDate, String email) {
        Client client = new Client();
        client.setName(name);
        client.setBirthDate(birthDate);
        client.setEmail(email);
        clientRepository.save(client);
    }
}
