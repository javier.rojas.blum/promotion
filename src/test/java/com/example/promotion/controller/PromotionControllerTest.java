package com.example.promotion.controller;

import com.example.promotion.dto.EmailsSentDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PromotionControllerTest {

    @Autowired
    PromotionController promotionController;

    @Test
    void sendEmails() {
        EmailsSentDto emailsSent = WebTestClient
                .bindToController(promotionController)
                .build()
                .mutate()
                .responseTimeout(Duration.ofMillis(30000))
                .build()
                .get()
                .uri("/birthdate")
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody(EmailsSentDto.class)
                .returnResult()
                .getResponseBody();

        System.out.println(emailsSent);

        assertNotNull(emailsSent);
        assertTrue(emailsSent.getCount() > 0);
    }
}