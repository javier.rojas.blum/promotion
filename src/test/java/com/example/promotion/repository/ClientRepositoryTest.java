package com.example.promotion.repository;

import com.example.promotion.model.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class ClientRepositoryTest {

    @Autowired
    ClientRepository clientRepository;

    @Test
    void findAll() {
        List<Client> clients = clientRepository.findAll();

        assertNotNull(clients);
        assertTrue(clients.size() > 0);

        for (Client client : clients) {
            System.out.println(client);
        }
    }
}