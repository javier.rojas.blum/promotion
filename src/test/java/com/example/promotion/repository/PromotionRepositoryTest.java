package com.example.promotion.repository;

import com.example.promotion.model.Promotion;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PromotionRepositoryTest {

    @Autowired
    PromotionRepository promotionRepository;

    @Test
    void findAll() {
        List<Promotion> promotions = promotionRepository.findAll();

        assertNotNull(promotions);
        assertTrue(promotions.size() > 0);

        for (Promotion promotion : promotions) {
            System.out.println(promotion);
        }
    }
}