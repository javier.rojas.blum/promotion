package com.example.promotion.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class SpringMailConfigTest {

    @Value("${spring.mail.username}")
    private String emailUsername;

    @Value("${spring.mail.password}")
    private String emailPasswd;

    @Test
    void environmentVariablesTest() {
        assertNotNull(emailUsername);
        assertNotNull(emailPasswd);
    }
}
