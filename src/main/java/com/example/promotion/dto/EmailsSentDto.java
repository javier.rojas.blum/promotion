package com.example.promotion.dto;

import lombok.Data;

@Data
public class EmailsSentDto {

    private int count;
}
