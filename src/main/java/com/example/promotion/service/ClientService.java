package com.example.promotion.service;

import com.example.promotion.model.Client;
import com.example.promotion.repository.ClientRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ClientService {

    private ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> getByMonthAndDay(LocalDate localDate) {
        int month = localDate.getMonthValue();
        int day = localDate.getDayOfMonth();
        return clientRepository.getByMonthAndDay(month, day);
    }
}
