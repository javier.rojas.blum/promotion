package com.example.promotion.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailSenderService {

    private JavaMailSender emailSender;

    @Value("${promotion.mail.from}")
    private String mailFrom;

    public MailSenderService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    //@Async
    protected void sendEmail(String emailTo, String subject, String messageContent) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailFrom);
        message.setTo(emailTo);
        message.setSubject(subject);
        message.setText(messageContent);
        emailSender.send(message);
    }
}
