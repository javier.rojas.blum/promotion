package com.example.promotion.service;

import com.example.promotion.dto.EmailsSentDto;
import com.example.promotion.model.Client;
import com.example.promotion.model.Promotion;
import com.example.promotion.repository.PromotionRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class PromotionService {

    private PromotionRepository promotionRepository;

    private ClientService clientService;
    private MailSenderService mailSenderService;

    @Value("${promotion.discountValue}")
    private Integer discountValue;

    public PromotionService(PromotionRepository promotionRepository, ClientService clientService, MailSenderService mailSenderService) {
        this.promotionRepository = promotionRepository;
        this.clientService = clientService;
        this.mailSenderService = mailSenderService;
    }

    public EmailsSentDto sendBirthDayPromotion() {
        EmailsSentDto emailsSentDto = new EmailsSentDto();
        Optional<Promotion> promotionOptional = promotionRepository.findById(1L);

        if (promotionOptional.isPresent()) {
            List<Client> clients = clientService.getByMonthAndDay(LocalDate.now());

            for (Client client : clients) {
                String subject = promotionOptional.get().getSubject()
                        .replace("<discountValue>", discountValue.toString());
                String messageContent = promotionOptional.get().getDescription()
                        .replace("<name>", client.getName())
                        .replace("<discountValue>", discountValue.toString());

                mailSenderService.sendEmail(client.getEmail(), subject, messageContent);
            }

            emailsSentDto.setCount(clients.size());
        }

        return emailsSentDto;
    }
}
