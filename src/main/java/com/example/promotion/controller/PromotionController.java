package com.example.promotion.controller;

import com.example.promotion.dto.EmailsSentDto;
import com.example.promotion.service.PromotionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/promotions")
public class PromotionController {

    private PromotionService promotionService;

    public PromotionController(PromotionService promotionService) {
        this.promotionService = promotionService;
    }

    @GetMapping("/birthdate")
    public EmailsSentDto sendEmails() {
        return promotionService.sendBirthDayPromotion();
    }
}
