package com.example.promotion.repository;

import com.example.promotion.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(value = "select * from CLIENT c where month(c.BIRTH_DATE) = :month and day(c.BIRTH_DATE) = :day", nativeQuery = true)
    List<Client> getByMonthAndDay(@Param("month") int month, @Param("day") int day);
}
