package com.example.promotion.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class Promotion extends ModelBase {

    @Column(nullable = false)
    private String subject;

    @Column(nullable = false)
    private String description;
}
